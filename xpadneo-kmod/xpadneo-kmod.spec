%global buildforkernels akmod
%global debug_package %{nil}
%global forgeurl https://github.com/atar-axis/xpadneo
%global prjname xpadneo

Name: %{prjname}-kmod
Version: 0.9.7
Release: 1%{?dist}
Summary: Kernel module (kmod) for %{prjname}

%forgemeta

License: GPLv3
URL: https://atar-axis.github.io/xpadneo
Source0: %{forgesource}

BuildRequires: gcc
BuildRequires: make
BuildRequires: kmodtool

# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo %{repo} --kmodname %{prjname} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null | sed '/global with_rhel_kabi/d') }

%description
Advanced Linux Driver for Xbox One Wireless Controller (shipped with Xbox One
S).

  * Supports Bluetooth
  * Supports all Force Feedback/Rumble effects through Linux ff-memless effect
    emulation
  * Supports Trigger Force Feedback in every game by applying a
    pressure-dependent effect intensity to the current rumble effect (not even
    supported in Windows)
  * Supports disabling FF
  * Supports multiple Gamepads at the same time (not even supported in
    Windows)
  * Offers a consistent mapping, even if the Gamepad was paired to
    Windows/Xbox before, and independent of software layers (SDL2, Stadia via
    Chrome Gamepad API, etc)
  * Working Select, Start, Mode buttons
  * Correct Axis Range (signed, important for e.g. RPCS3)
  * Supports Battery Level Indication (including the Play 'n Charge Kit)
    Battery Level Indication
  * Easy Installation
  * Agile Support and Development
  * Supports customization through profiles (work in progress)
  * Optional high-precision mode for Wine/Proton users
  * Share button support on supported controllers
  * Works as a mouse if you're are in couch-mode (press Guide+Select)

This package contains the kmod module for %{prjname}.


%prep
%forgeautosetup -p1

# Error out if there was something wrong with kmodtool.
%{?kmodtool_check}

# Print kmodtool output for debugging purposes:
kmodtool --target %{_target_cpu} --repo %{repo} --kmodname %{prjname} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null | sed '/global with_rhel_kabi/d'

for kernel_version in %{?kernel_versions} ; do
    cp -a hid-%{prjname} _kmod_build_${kernel_version%%___*}
done

echo %{version} > VERSION


%build
for kernel_version in %{?kernel_versions} ; do
    make V=1 %{?_smp_mflags} -C ${kernel_version##*___} M=${PWD}/_kmod_build_${kernel_version%%___*}/src modules
done


%install
for kernel_version in %{?kernel_versions}; do
    mkdir -p %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
    install -Dpm 0755 _kmod_build_${kernel_version%%___*}/src/*.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
    chmod a+x %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/*.ko
done
%{?akmod_install}


%changelog
* Wed Dec 25 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.7-1
- chore: Bump to v0.9.7

* Mon Dec 23 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.6-4
- fix: Actually fix building with custom kernels on EL

* Thu Dec 19 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.6-3
- fix: Build on 6.12 kernel
- fix: Rollback to akmod on EL so module will work with custom kernels

* Sat Sep 28 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.6-2
- fix: Build kmod instead of akmod for EL

* Mon Apr 22 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.6-1
- chore: Update to v0.9.6

* Mon Sep 26 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.5-1
- chore: Update to v0.9.5

* Mon Jun 27 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.4-2
- chore: Fix wrong license

* Mon Jun 27 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.4-1
- Update to v0.9.4

* Mon May 30 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.3-1
- Update to v0.9.3

* Mon May 30 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.2-2
- Change --repo arg for kmodtool

* Thu May 26 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.9.2-1
- chore(update): 0.9.2

* Tue Nov 02 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.9.1-3
- build: Polish build to conform packaging guidelines

* Fri Jul 16 2021 Dorian Stoll <dorian.stoll@tmsp.io> - 0.9.1-1
- Initial package
