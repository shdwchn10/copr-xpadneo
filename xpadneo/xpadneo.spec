%global debug_package %{nil}
%global forgeurl https://github.com/atar-axis/xpadneo

Name: xpadneo
Version: 0.9.7
Release: 1%{?dist}
Summary: Advanced linux driver for Xbox One Wireless Gamepad

%forgemeta

License: GPLv3
URL: https://atar-axis.github.io/xpadneo
Source0: %{forgesource}

BuildArch: noarch

BuildRequires: gcc
BuildRequires: make
BuildRequires: systemd-rpm-macros

Requires: %{name}-kmod >= %{version}
Requires: bluez

Provides: %{name}-kmod-common = %{version}-%{release}

%description
Advanced Linux Driver for Xbox One Wireless Controller (shipped with Xbox One
S).

  * Supports Bluetooth
  * Supports all Force Feedback/Rumble effects through Linux ff-memless effect
    emulation
  * Supports Trigger Force Feedback in every game by applying a
    pressure-dependent effect intensity to the current rumble effect (not even
    supported in Windows)
  * Supports disabling FF
  * Supports multiple Gamepads at the same time (not even supported in
    Windows)
  * Offers a consistent mapping, even if the Gamepad was paired to
    Windows/Xbox before, and independent of software layers (SDL2, Stadia via
    Chrome Gamepad API, etc)
  * Working Select, Start, Mode buttons
  * Correct Axis Range (signed, important for e.g. RPCS3)
  * Supports Battery Level Indication (including the Play 'n Charge Kit)
    Battery Level Indication
  * Easy Installation
  * Agile Support and Development
  * Supports customization through profiles (work in progress)
  * Optional high-precision mode for Wine/Proton users
  * Share button support on supported controllers
  * Works as a mouse if you're are in couch-mode (press Guide+Select)


%prep
%forgeautosetup


%install
install -Dpm 0644 hid-%{name}/etc-modprobe.d/%{name}.conf %{buildroot}%{_modprobedir}/%{name}.conf
install -Dpm 0644 hid-%{name}/etc-udev-rules.d/60-%{name}.rules %{buildroot}%{_udevrulesdir}/60-%{name}.rules
install -Dpm 0644 hid-%{name}/etc-udev-rules.d/70-%{name}-disable-hidraw.rules %{buildroot}%{_udevrulesdir}/70-%{name}-disable-hidraw.rules


%files
%{_modprobedir}/%{name}.conf
%{_udevrulesdir}/60-%{name}.rules
%{_udevrulesdir}/70-%{name}-disable-hidraw.rules


%changelog
* Wed Dec 25 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.7-1
- chore: Bump to v0.9.7

* Sat Sep 28 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.6-2
- fix: Set BuildArch to noarch

* Mon Apr 22 2024 Andrey Brusnik <dev@shdwchn.io> - 0.9.6-1
- chore: Update to v0.9.6

* Mon Sep 26 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.5-1
- chore: Update to v0.9.5

* Mon Jun 27 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.4-2
- chore: Fix wrong license

* Mon Jun 27 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.4-1
- Update to v0.9.4

* Mon May 30 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.3-2
- Remove bluez-tools dependency since bluetoothctl is in the bluez package

* Mon May 30 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.3-1
- Update to v0.9.3

* Mon May 30 2022 Andrey Brusnik <dev@shdwchn.io> - 0.9.2-2
- Add 50-xpadneo-fixup-steamlink.rules

* Thu May 26 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.9.2-1
- chore(update): 0.9.2

* Tue Nov 02 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.9.1-2
- build: Polish build to conform packaging guidelines

* Fri Jul 16 2021 Dorian Stoll <dorian.stoll@tmsp.io> - 0.9.1-1
- Initial package
