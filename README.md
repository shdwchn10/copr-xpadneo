# xpadneo package for copr
Unofficial [xpadneo](https://github.com/atar-axis/xpadneo) akmod built for current supported Fedora Linux and Enterprise Linux releases. [Link to Copr repo](https://copr.fedorainfracloud.org/coprs/shdwchn10/xpadneo/).

## Installation

Classic Fedora Linux with dnf, RHEL, EL clones or CentOS Stream:
```
sudo dnf copr enable shdwchn10/xpadneo

sudo dnf install xpadneo

systemctl reboot
```

Fedora Atomic Desktops/IoT/CoreOS 41+:
```
sudo dnf copr enable shdwchn10/xpadneo

rpm-ostree install xpadneo

systemctl reboot
```

Fedora Atomic Desktops/IoT/CoreOS 40 or older:
```
sudo wget -O /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:shdwchn10:xpadneo.repo https://copr.fedorainfracloud.org/coprs/shdwchn10/xpadneo/repo/fedora-$(rpm -E %fedora)/shdwchn10-xpadneo-fedora-$(rpm -E %fedora).repo

rpm-ostree install xpadneo

systemctl reboot
```
